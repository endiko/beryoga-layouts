module.exports = [
    './gulp/tasks/sass.js',
    './gulp/tasks/pug.js',
    './gulp/tasks/clean.js',
    './gulp/tasks/fonts.js',
    './gulp/tasks/images.js',
    './gulp/tasks/svg.js',
    './gulp/tasks/serve.js',
    './gulp/tasks/watch.js'
];