module.exports = function () {
    $.gulp.task('watch', function () {
        $.gulp.watch('./source/images/**/*.*', $.gulp.parallel('svg', 'images'))
        $.gulp.watch('./source/styles/**/*.scss', $.gulp.parallel('sass'))
        // .on('change', $.browserSync.stream());
        $.gulp.watch('./source/template/**/*.pug', $.gulp.parallel('pug'))
        // .on('change', $.browserSync.reload);
    });
};