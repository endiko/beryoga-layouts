module.exports = function () {
    $.gulp.task('images', function () {
        return $.gulp.src('./source/images/**/*')
            .pipe($.gulp.dest('./build/images/'))
    });
};

