module.exports = function () {
    $.gulp.task('fonts', function () {
        return $.gulp.src('./source/fonts/*.woff')
            .pipe($.gulp.dest('./build/fonts/'))
    });
};

