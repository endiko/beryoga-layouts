module.exports = function () {
    $.gulp.task('pug', function () {
        return $.gulp.src('./source/template/views/pages/**/*.pug')
            .pipe($.gulpLoadPlugins.pug({ pretty: true }))
            .on('error', $.gulpLoadPlugins.notify.onError(function (error) {
                return {
                    title: 'Pug',
                    message: error.message
                }
            }))
            .pipe($.gulp.dest('./build/'))
    });
};

