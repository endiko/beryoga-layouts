module.exports = function () {
    $.gulp.task('sass', function () {
        return $.gulp.src('./source/styles/**/*.scss')
            .pipe($.gulpLoadPlugins.sourcemaps.init())
            .pipe($.gulpLoadPlugins.sass({ includePaths: require('node-normalize-scss').includePaths }))
            .on('error', $.gulpLoadPlugins.notify.onError({ title: 'Style' }))
            .pipe($.gulpLoadPlugins.autoprefixer({
                browsers: [
                    'last 3 version',
                    '> 1%',
                    'ie 8',
                    'ie 9',
                    'Opera 12.1'
                ]
            }))
            .pipe($.cssunit({
                type: 'px-to-rem',
                rootSize: 16
            }))
            .pipe($.gulpLoadPlugins.sourcemaps.write())
            .pipe($.gulp.dest('./build/css'))
            .pipe($.browserSync.stream())
    });
}

