module.exports = function () {
    $.gulp.task('svg', function () {
        return $.gulp.src('./source/images/svg/*.svg')
            .pipe($.gulp.dest('./build/images/svg/'))
    });
};

