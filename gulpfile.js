global.$ = {
    path: {
        task: require('./gulp/path/tasks')
    },
    gulp: require('gulp'),
    del: require('del'),
    cssunit: require('gulp-css-unit'),
    browserSync: require('browser-sync'),
    gulpLoadPlugins: require('gulp-load-plugins')()
};

$.path.task.forEach(function (taskPath) {
    require(taskPath)();
});

$.gulp.task('default', $.gulp.series(
    'clean',
    $.gulp.parallel('sass', 'pug', 'fonts', 'images', 'serve', 'watch')
    // $.gulp.parallel('watch', 'serve')
));